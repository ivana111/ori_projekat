import os
import shutil
from keras.optimizers import Adam
from keras.preprocessing.image import ImageDataGenerator
import matplotlib.pyplot as plt
from keras.layers import Conv2D, MaxPooling2D, Dropout, Flatten, Dense
from keras.models import Sequential
import warnings
warnings.filterwarnings('always')
warnings.filterwarnings('ignore')

labels = os.listdir("data/flowers")
MAIN_DIR = 'data/flowers'
TRAIN_DIR = 'data/train'
VALID_DIR = 'data/valid'

BATCH_SIZE = 128
IMAGE_SHAPE = 150
EPOCHS = 60


# dijelimo podatke na skup za treniranje i skup za validaciju
def load_data():
    for label in labels:
        source = os.path.join(MAIN_DIR, label)
        images = os.listdir(source)

        split = int(len(images) * 0.8)  # 80% za treniranje
        train, valid = images[:split], images[split:]

        for image in train:
            destination = os.path.join(TRAIN_DIR, label)
            os.makedirs(destination, exist_ok=True)
            shutil.copy(os.path.join(source, image), destination)

        for image in valid:
            destination = os.path.join(VALID_DIR, label)
            os.makedirs(destination, exist_ok=True)
            shutil.copy(os.path.join(source, image), destination)


def generate_data():
    train_generator = ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True,
        rescale=1./255
    )

    train_data = train_generator.flow_from_directory(
        directory=TRAIN_DIR,
        target_size=(IMAGE_SHAPE, IMAGE_SHAPE),
        batch_size=BATCH_SIZE,
        shuffle=True,
        class_mode='binary'
    )

    valid_generator = ImageDataGenerator(rescale=1./255)
    valid_data = valid_generator.flow_from_directory(
        directory=VALID_DIR,
        target_size=(IMAGE_SHAPE, IMAGE_SHAPE),
        batch_size=BATCH_SIZE,
        class_mode='binary',
        shuffle=False
    )

    return train_data, valid_data


def make_model(train_data, valid_data):
    model = Sequential([
        Conv2D(
            filters=32,
            kernel_size=(5, 5),
            activation='relu',
            padding='same',
            input_shape=(IMAGE_SHAPE, IMAGE_SHAPE, 3)
        ),
        MaxPooling2D(pool_size=2, strides=2),

        Conv2D(64, (3, 3), activation='relu', padding='same'),
        MaxPooling2D(2, 2),

        Conv2D(96, (3, 3), activation='relu', padding='same'),
        MaxPooling2D(2, 2),

        Conv2D(filters=96, kernel_size=(3, 3), padding='Same', activation='relu'),
        MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),

        Dropout(0.2),
        Flatten(),
        Dense(units=512, activation='relu'),
        Dense(units=5, activation='softmax')
        ])

    model.compile(optimizer=Adam(lr=0.001),
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])
    model.summary()

    history = model.fit_generator(generator=train_data,
                                  steps_per_epoch=(train_data.n + BATCH_SIZE - 1) // BATCH_SIZE,
                                  epochs=EPOCHS,
                                  validation_data=valid_data,
                                  validation_steps=(valid_data.n + BATCH_SIZE - 1) // BATCH_SIZE,
                                  verbose=1
                                  )

    model.save('model/model.h5')

    plt.figure(figsize=(12, 8))
    plt.subplot(1, 2, 1)
    plt.plot(range(EPOCHS), history.history['accuracy'], label='train')
    plt.plot(range(EPOCHS), history.history['val_accuracy'], label='valid')
    plt.legend(loc='lower right')
    plt.title('Accuracy')

    plt.show()


if __name__ == '__main__':
    load_data()
    train_data, valid_data = generate_data()
    make_model(train_data, valid_data)
